#Future NOW Crew Utilities

## Installation
    $ npm install fnc-utils --save

## Usage

```js
const Utility = require('fnc-utils');

// Example Discord command
var command = 'test-command -f some content'

/* From the Master Bot */
var masterCommand = Utility.createBotCommand(command);
// {"name":"test-command","id":"12345","flags":[ f ],"content":"some content"}


/* From the Slave Bot */
var slaveCommand = Utility.parseBotCommand(masterCommand);
// { name: 'test-command',
//   id: '12345',
//   flags: [ f ],
//   content: 'some content' }
```

## Authors

* **Ed Fabre** - *Dev* - [Ed's Github](https://github.com/EdFabre) | [Ed's Bitbucket](https://bitbucket.org/enfabre/)
* **Ricky Sokel** - *Dev* - [Ricky's Github](https://github.com/vulpcod3z) | [Ricky's Bitbucket](https://bitbucket.org/vulpz/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

TBA
