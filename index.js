/**
 * The Master bot will use this function on all incoming commands to break them
 * down into an easy to read JSON object for slave bots.
 *
 * @param  {string} inCommand [description]
 * @return {[type]}           [description]
 */
function createBotCommand(inCommand) {
  var command = {};
  var cmdParsed = inCommand.split(' ');

  command.name = cmdParsed[0];
  command.id = require('crypto-random-string')(5);
  command.flags = [];
  tempCmdParsed = cmdParsed;
  for (var i = 0; i < cmdParsed.length; i++) {
    if (cmdParsed[i].startsWith('-')) {
      command.flags.push(cmdParsed[i].substring(1));
      var index = cmdParsed.indexOf(cmdParsed[i]);
      cmdParsed.splice(index, 1);
      i--;
    }
  }

  cmdParsed.shift();
  command.content = cmdParsed.join(' ');
  return JSON.stringify(command);
}

/**
 * This function takes a JSON String and converts back to a Javascript object.
 *
 * @param  {[type]} inCommandJSON [description]
 * @return {[type]}               [description]
 */
function parseBotCommand(inCommandJSON) {
  return JSON.parse(inCommandJSON)
}

module.exports = {
  createBotCommand,
  parseBotCommand
};
